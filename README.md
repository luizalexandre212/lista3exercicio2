# Lista3Exercicio2

/*2) Fazer um programa para ler o salário de uma pessoa, o percentual de aumento e o percentual de descontos. Os descontos incidem sobre o salário com aumento. Calcular o novo salário e mostrá-lo como no exemplo a seguir. Exemplo:
 */

 #include<stdio.h>
 #include<stdlib.h>

 main()
 {
     double salario, peral, perde, salal, sall,salint,cent;
     printf("Informe o valor do salario:\n ");
     scanf("%lf", &salario);
     printf("Informe o percentual de aumento(Use 10 para 10%%): \n");
     scanf("%lf", &peral);
     printf("Informe o percentual de desconto(Use 7 para 7%%): \n");
     scanf("%lf", &perde);

     salal=salario+(salario*(peral/100));
     printf("Salario aumentado: %.2lf\n",salal);
    sall=salal-(salal*(perde/100));
     printf("Salario liquido: %.2lf\n",sall);
    salint=(int)sall;
    cent=sall-salint;
     printf("O salario liquido e: %.0lf reais e %.0lf centavos",salint, cent*100 );
 }
